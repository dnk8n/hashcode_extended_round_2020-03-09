from typing import List, Callable, Tuple, Optional
from deap import base, creator, tools, algorithms
import numpy as np
import random
from functools import reduce
import time

def crossover_generic_ordered_lists(list1, list2):
    """Performs ordered crossover on copies of original lists.  
    Does not mutate lists in place.

    Helper function.
    """
    reference_list = sorted(list1)
    list1_indices = [list1.index(ref_indx) for ref_indx in reference_list]
    list2_indices = [list2.index(ref_indx) for ref_indx in reference_list]

    new_indices_lists = tools.cxOrdered(
        list1_indices.copy(),  # shouldn't need to .copy() here, but whatever
        list2_indices.copy()
        )
    new_list1, new_list2 = list(map(
        lambda x: [reference_list.__getitem__(i) for i in x],
        new_indices_lists
        ))
    return new_list1, new_list2

def mate_lists(ind1: List[List], ind2: List[List], map_func=None) -> Tuple[List[List]]:
    map_ = map_func if not map_func is None else map
    new_lists = list(map_(crossover_generic_ordered_lists, ind1, ind2))
    new_book_lists1, new_book_lists2 = [list(x) for x in zip(*new_lists)]
    return new_book_lists1, new_book_lists2

def mutate_lists(ind: List[List], mutate_prob: float, map_func=None):
    map_ = map_func if not map_func is None else map
    lists_new = list(map_(
        lambda x: tools.mutShuffleIndexes(
            x.copy(), mutate_prob
            )[0],
            ind
        ))
    return lists_new,

# initialise population (or individuals) can be made smart
def create_random_lists(library_and_book_options: List[List], **kwargs) -> List[List]:
    """Creates a random initial population member for the EA using the features 
    from the problem specification.
    """
    copied_lists = [inner_list.copy() for inner_list in library_and_book_options]
    unshuffled = [inner_list.copy() for inner_list in library_and_book_options]
    for inner_list in copied_lists:
        random.shuffle(inner_list)  # shuffles in place
    if random.random() < 0.02:
        return unshuffled
    else:
        return copied_lists

# we need to change the evaluate function
def test_evaluate(ind: List[List]):
    scores = [
        reduce(lambda x,y: x + y[1]/(y[0]), enumerate(inner_list, 1), 0)
        for inner_list in ind
        ]
    score = sum(scores)
    return score,


def run_EA(
    library_and_book_options: List[List],
    fitness_function: Callable[[List[List]], Tuple[float]] = test_evaluate,
    mutate_prob: float = 0.1,
    crossover_prob: float = 0.1,
    pop_size: int = 40,
    num_generations: int = 100,
    use_multiprocessing: bool = False,  # for parallelisation, doesn't seem to work
    use_dask: bool = False,  # for parallelisation
    verbose: bool = False,
    max_run_time: Optional[float] = None,
    **initialisation_kwargs
    ):
    """Returns:  
    Final population: List[List[List]]  
    Logbook: Dict (I think)  
    Best seen candidate: List[List]
    """
    # Which parallelisation option we choose determines which map function is used
    if use_multiprocessing:
        # This doesn't seem to work (yet?
        # Try with Dask
        import multiprocessing

        multiprocessing_pool = multiprocessing.Pool()
        map_ = multiprocessing_pool.map
    elif use_dask:
        # Note: dask map function only takes a single iterable.
        # Hence, it needs to be reconfigured to speed up mating with individuals with
        # many chromosomes.
        import dask.bag as db

        def map_(func, iterable):
            bag = db.from_sequence(iterable).map(func)
            return bag.compute()
    else:
        map_ = map
    
    ###
    # If run_EA is run more than once, this code will be duplicate because it creates classes.
    # This means RuntimeWarnings will be raised. They can probably be safely ignored.
    creator.create("Fitness", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.Fitness)
    ###

    def individual_wrapper(nested=False):
        def decorator(func):
            def call(*args, **kwargs):
                result = func(*args, **kwargs)
                if nested:
                    result = [creator.Individual(x) for x in result]
                else:
                    result = creator.Individual(result)
                return result
            return call
        return decorator

    toolbox = base.Toolbox()

    toolbox.register("map", map_)

    create_individual = individual_wrapper(nested=False)(create_random_lists)
    toolbox.register("individual", create_individual,
        library_and_book_options=library_and_book_options)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    mate = individual_wrapper(nested=True)(mate_lists)
    toolbox.register("mate", mate)

    mutate = individual_wrapper(nested=True)(mutate_lists)
    # Faster to NOT use the map_func=map_ argument for Dask
    # toolbox.register("mutate", mutate, mutate_prob=mutate_prob, map_func=map_)
    toolbox.register("mutate", mutate, mutate_prob=mutate_prob)

    toolbox.register("select", tools.selTournament, tournsize=min(10, pop_size))
    toolbox.register("evaluate", fitness_function)

    stats = tools.Statistics(key=lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    halloffame = tools.HallOfFame(maxsize=1)

    population = toolbox.population(n=pop_size)

    if max_run_time is None:
        population, logbook = algorithms.eaSimple(
            population,
            toolbox,
            crossover_prob,
            mutate_prob,
            num_generations,
            stats=stats,
            halloffame=halloffame,
            verbose=verbose
            )
    else:
        start = time.time()
        remaining_generations = num_generations
        while remaining_generations > 0 and time.time() - start < max_run_time:
            # do at most 10 generations at a time
            num_generations_for_now = min(remaining_generations, 10)
            remaining_generations = remaining_generations - num_generations_for_now
            population, logbook = algorithms.eaSimple(
                population,
                toolbox,
                crossover_prob,
                mutate_prob,
                num_generations_for_now,
                stats=stats,
                halloffame=halloffame,
                verbose=verbose
                )

    # if verbose:
    #     print("Best candidate seen:", halloffame[0])
    if use_multiprocessing:
        multiprocessing_pool.close()
    return population, logbook, halloffame[0]


if __name__ == '__main__':
    import time
    
    library_and_book_options = [list(range(20)) for i in range(20)]
    pop_size = 1000
    num_generations = 10
    mutate_prob = 0.5
    crossover_prob = 0.5

    dask_start = time.time()
    run_EA(
        library_and_book_options,
        pop_size=pop_size,
        mutate_prob=mutate_prob,
        crossover_prob=crossover_prob,
        num_generations=num_generations,
        use_dask=True,
        verbose=True
        )
    dask_end = time.time()

    singlethread_start = time.time()
    run_EA(
        library_and_book_options,
        pop_size=pop_size,
        mutate_prob=mutate_prob,
        crossover_prob=crossover_prob,
        num_generations=num_generations,
        use_dask=False,
        verbose=True
        )
    singlethread_end = time.time()

    print("Dask time:", dask_end - dask_start)
    print("Single-thread time:", singlethread_end - singlethread_start)
