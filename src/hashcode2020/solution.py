import copy
import itertools
import pathlib
import random

from hashcode2020 import extract_ints, lazy_property, pairwise


class SolutionHelper(object):
    def __init__(self, problem_obj):
        self.problem = problem_obj

    def eval_solutions(self, options):
        return Solution(options, self.problem).get_fitness()


class Solution(object):
    """
    Takes in an individual (an encoded potential solution to the problem) and produces an object, from which
    a submission file can be produced. The score for that solution is also calculated. This can be made use
    of within an optimization algorithm or for the final proposed solution
    """
    def __init__(self, source, problem_obj=None, is_refined=False):
        self.problem = problem_obj
        self.source = copy.deepcopy(source)
        self.is_refined = is_refined

    @lazy_property
    def refined(self):
        if self.is_refined:
            return copy.deepcopy(self.source)
        if not self.problem:
            raise ValueError("Problem attribute is required in order to calculated refined proposed solution")
        problem_copy = copy.deepcopy(self.problem)
        source_copy = copy.deepcopy(self.source)
        library_sign_order = source_copy[0]
        libraries_scanning = []
        book_scan_order_by_library = [list() for _ in range(problem_copy.num_libraries)]
        unique_books_scanned = set()
        current_library_sign_start_day = 0
        current_library = None
        for day in range(problem_copy.num_days):
            if day == current_library_sign_start_day:
                if current_library is not None:
                    libraries_scanning.append(current_library)
                try:
                    current_library = library_sign_order.pop(0)
                except IndexError:
                    pass
                else:
                    current_library_sign_start_day += problem_copy.libraries[current_library].num_signup_days
            libraries_finished_scanning = []
            for library_idx in libraries_scanning:
                lib = problem_copy.libraries[library_idx]
                if len(self.source) == 1:
                    lib_books = lib.books_ordered_by_score
                else:
                    lib_books = source_copy[1:][library_idx]
                books_popped = 0
                while books_popped < lib.num_books_per_day:
                    try:
                        book_considered = lib_books.pop(0)
                    except IndexError:
                        libraries_finished_scanning.append(library_idx)
                        break
                    else:
                        if book_considered not in unique_books_scanned:
                            books_popped += 1
                            book_scan_order_by_library[library_idx].append(book_considered)
                            unique_books_scanned.add(book_considered)
            for library_finished_scanning in libraries_finished_scanning:
                libraries_scanning.remove(library_finished_scanning)
        source = self.source[0]
        for idx, library in enumerate(book_scan_order_by_library):
            if not library:
                source.remove(idx)
        return [source] + book_scan_order_by_library

    @lazy_property
    def libraries_signed(self):
        return self.refined[0]

    @lazy_property
    def num_libraries_signed(self):
        return len(self.libraries_signed)

    @lazy_property
    def book_order_by_library(self):
        return self.refined[1:]

    @lazy_property
    def fitness(self):
        if not self.problem:
            raise ValueError("Problem attribute is required in order to calculated solution's fitness")
        unique_book_indices = set(itertools.chain(*self.book_order_by_library))
        fitness_score = sum(map(lambda idx: self.problem.book_scores[idx], unique_book_indices))
        return fitness_score

    def save_as_submission(self, output_file):
        output_path = pathlib.Path(output_file)
        with output_path.open('w') as f:
            f.write(str(self.num_libraries_signed))
            f.write('\n')
            for lib in self.libraries_signed:
                f.write(f'{lib} {len(self.book_order_by_library[lib])}')
                f.write('\n')
                f.write(' '.join(map(str, self.book_order_by_library[lib])))
                f.write('\n')

    def get_fitness(self):
        return tuple([int(self.fitness)])

    @classmethod
    def create_solution(cls, problem, is_random=True):
        problem_library_order = list(range(problem.num_libraries))
        problem_libraries = problem.libraries
        if is_random:
            random_library_order = copy.copy(problem_library_order)
            random_libraries = copy.deepcopy(problem_libraries)
            random.shuffle(random_library_order)
            random_book_order = []
            for lib in random_libraries:
                random.shuffle(lib.books)
                random_book_order.append(lib.books)
            library_order = random_library_order
            book_order = random_book_order
        else:
            problem_book_order = [lib.books for lib in problem_libraries]
            library_order = problem_library_order
            book_order = problem_book_order
        return cls([library_order] + book_order, problem)

    @classmethod
    def load_submission(cls, input_file):
        input_path = pathlib.Path(input_file)
        lines = input_path.read_text().splitlines()
        libraries = []
        books = []
        for line_0, line_1 in pairwise(lines[1:]):
            libraries.append(list(extract_ints(line_0))[0])
            books.append(list(extract_ints(line_1)))
        return cls([libraries] + books, is_refined=True)
