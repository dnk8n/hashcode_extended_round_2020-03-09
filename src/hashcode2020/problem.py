import pathlib

import numpy as np

from hashcode2020 import extract_ints, lazy_property, pairwise


class Library(object):
    def __init__(self, num_books, num_signup_days, num_books_per_day, books, books_ordered_by_score, priority):
        self.num_books = num_books
        self.num_signup_days = num_signup_days
        self.num_books_per_day = num_books_per_day
        self.books = books
        self.books_ordered_by_score = books_ordered_by_score
        self.priority = priority


class Problem(object):
    __files = (
        'a_example.txt',
        'b_read_on.txt',
        'c_incunabula.txt',
        'd_tough_choices.txt',
        'e_so_many_books.txt',
        'f_libraries_of_the_world.txt'
    )
    __paths = [pathlib.Path(__file__).resolve().parent / 'problem_files' / file for file in __files]

    def __init__(self, input_path):
        self.__lines = input_path.read_text().splitlines()
        self.input_path = input_path
        self.num_unique_books, self.num_libraries, self.num_days = extract_ints(self.__lines[0])
        self.book_scores = np.array(list(extract_ints(self.__lines[1])))

    @lazy_property
    def libraries(self):
        libs = []
        for line_0, line_1 in pairwise(self.__lines[2:]):
            books = np.array(list(extract_ints(line_1)))
            books_ordered_by_score = books[self.book_scores[books].argsort()[::-1]]
            num_books, num_signup_days, num_books_per_day = extract_ints(line_0)
            priority = sum(
                map(
                    lambda x: self.book_scores[x],
                    books_ordered_by_score[:(self.num_days - num_signup_days) * num_books_per_day]
                )
            )
            libs.append(
                Library(
                    num_books, num_signup_days, num_books_per_day, books.tolist(), books_ordered_by_score.tolist(),
                    priority
                )
            )
        return libs

    @lazy_property
    def libraries_ordered_by_priority(self):
        priorities = np.array([lib.priority for lib in self.libraries])
        original_library_order = np.array(range(self.num_libraries))
        priority_library_order = original_library_order[priorities.argsort()[::-1]]
        return priority_library_order.tolist()

    @lazy_property
    def books_ordered_by_score_per_library(self):
        return [lib.books_ordered_by_score for lib in self.libraries]

    @classmethod
    def load_problem_by_letter(cls, letter):
        return cls(cls.__paths[ord(letter) - 97])
