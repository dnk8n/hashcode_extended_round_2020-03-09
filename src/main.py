import pathlib

from hashcode2020.problem import Problem
from hashcode2020.solution import Solution

total_score = 0
for letter in ['a', 'b', 'c', 'd', 'e', 'f']:
    problem = Problem.load_problem_by_letter(letter)
    priority_library_order = problem.libraries_ordered_by_priority
    hand_calculated_solution = Solution([priority_library_order], problem)
    output_dir_path = pathlib.Path(__file__).resolve().parent / 'solution_files'
    output_file_path = output_dir_path / f'solution_main_score-{hand_calculated_solution.fitness}_{problem.input_path.name}'
    hand_calculated_solution.save_as_submission(output_file_path)
    total_score += hand_calculated_solution.fitness
    print(f'score_{letter}: {hand_calculated_solution.fitness}')
print('total_score:', total_score)