import pathlib

from hashcode2020.problem import Problem
from hashcode2020.solution import Solution, SolutionHelper
from hashcode2020.evolutionalgo import run_EA

total_score = 0
for letter in ['a', 'b', 'c', 'd', 'e', 'f']:
    problem = Problem.load_problem_by_letter(letter)
    priority_library_order = problem.libraries_ordered_by_priority
    books_ordered_by_score_per_library = problem.books_ordered_by_score_per_library
    solution_helper = SolutionHelper(problem)

    # If + books_ordered_by_score_per_library is ommitted here, the fitness functions knows to assume books in order of
    # value, else if + books_ordered_by_score_per_library is included, it analyses the value of books in order provided
    population, logbook, top_solution = run_EA(
        library_and_book_options=[priority_library_order] + books_ordered_by_score_per_library, fitness_function=solution_helper.eval_solutions,
        verbose=True, elitism=5, pop_size=100
    )
    solution = Solution(top_solution, problem)
    output_dir_path = pathlib.Path(__file__).resolve().parent / 'solution_files'
    output_file_path = output_dir_path / f'solution_ea_score-{solution.fitness}_{problem.input_path.name}'
    solution.save_as_submission(output_file_path)
    total_score += solution.fitness
    print(f'score_{letter}: {solution.fitness}')
print('total_score:', total_score)
